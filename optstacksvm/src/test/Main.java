package test;

import java.io.BufferedReader;
import java.io.FileReader;

import weka.classifiers.functions.OptStackSVM;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ArffLoader.ArffReader;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.StringToWordVector;

public class Main {

	public static void main(String[] args) {

		OptStackSVM opt = new OptStackSVM();
		try {
			BufferedReader reader =
					new BufferedReader(new FileReader("C:/Users/CUKANOVIC/Desktop/SerbMR-3C.arff"));
			ArffReader arff = new ArffReader(reader);
			Instances data = arff.getData();
			data.setClassIndex(data.numAttributes() - 1);
			StringToWordVector stringToWord = new StringToWordVector();
		    stringToWord.setAttributeIndices("1");
		    stringToWord.setLowerCaseTokens(true);
		    stringToWord.setTFTransform(true);
		    stringToWord.setOutputWordCounts(true);
		    stringToWord.setWordsToKeep(10000);
		    stringToWord.setInputFormat(data);
		    data = Filter.useFilter(data, stringToWord);

			opt.buildClassifier(data);
			
			for (Instance instance: data)
				opt.classifyInstance(instance);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
