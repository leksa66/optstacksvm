package weka.classifiers.functions;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Random;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.stream.IntStream;

import de.bwaldvogel.liblinear.SolverType;
import weka.classifiers.AbstractClassifier;
import weka.core.Attribute;
import weka.core.Capabilities;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.Option;
import weka.core.SelectedTag;
import weka.core.Tag;
import weka.core.Utils;
import weka.filters.Filter;
import weka.filters.unsupervised.instance.RemoveWithValues;

/**
<!-- globalinfo-start -->
* An optimal stack of LibLINEAR binary classifiers implementation. <br><br>
*	<p/>
<!-- globalinfo-end -->
*
 <!-- options-start -->
 * Valid options are: <p/>
 *
 * <pre> -S &lt;int&gt;
 *  Set type of solver (default: 1)
 *   for multi-class classification
 *     0 -- L2-regularized logistic regression (primal)
 *     1 -- L2-regularized L2-loss support vector classification (dual)
 *     2 -- L2-regularized L2-loss support vector classification (primal)
 *     3 -- L2-regularized L1-loss support vector classification (dual)
 *     4 -- support vector classification by Crammer and Singer
 *     5 -- L1-regularized L2-loss support vector classification
 *     6 -- L1-regularized logistic regression
 *     7 -- L2-regularized logistic regression (dual)
 *  for regression
 *    11 -- L2-regularized L2-loss support vector regression (primal)
 *    12 -- L2-regularized L2-loss support vector regression (dual)
 *    13 -- L2-regularized L1-loss support vector regression (dual)</pre>
 *
 * <pre> -C &lt;double&gt;
 *  Set the cost parameter C
 *   (default: 1)</pre>
 *
 * <pre> -Z
 *  Turn on normalization of input data (default: off)</pre>
 *
 * <pre>
 * -L &lt;double&gt;
 *  The epsilon parameter in epsilon-insensitive loss function.
 *  (default 0.1)
 * </pre>
 *
 * <pre>
 * -I &lt;int&gt;
 *  The maximum number of iterations to perform.
 *  (default 0.1)
 * </pre>
 *
 * <pre> -P
 *  Use probability estimation (default: off)
 * currently for L2-regularized logistic regression only! </pre>
 *
 * <pre> -E &lt;double&gt;
 *  Set tolerance of termination criterion (default: 0.001)</pre>
 *
 * <pre> -W &lt;double&gt;
 *  Set the parameters C of class i to weight[i]*C
 *   (default: 1)</pre>
 *
 * <pre> -B &lt;double&gt;
 *  Add Bias term with the given value if &gt;= 0; if &lt; 0, no bias term added (default: 1)</pre>
 *
 * <pre> -D
 *  If set, classifier is run in debug mode and
 *  may output additional info to the console</pre>
 *
 <!-- options-end -->
 *
* @author Aleksandar Cukanovic
* @version 1.0.0
*/
public class OptStackSVM extends AbstractClassifier {

	private static final long serialVersionUID = -5528779441163733973L;
		
	public static final String REVISION = "1.0.0";

	/**
	 * Number of folds used for the nested cross-validation
	 */
	public static final int NUM_OF_FOLDS = 5;
	
	
	/**
	 * Optimal stack table used to classify instances
	 */
	private int optStackTableSize;
	private int[] optimalStackTable;	
	
	/**
	 * LibLINEAR models used for training instances
	 */
	private int numModels;
	private LibLINEAR[] models;
	
	/**
	 * Class attribute
	 */
	private int numClasses;
	private Attribute classAttr;
	
	  /** normalize input data */
    protected boolean              m_Normalize            = false;

    /** SVM solver types */
    public static final Tag[]      TAGS_SVMTYPE           = {new Tag(SolverType.L2R_LR.getId(), "L2-regularized logistic regression (primal)"),
            new Tag(SolverType.L2R_L2LOSS_SVC_DUAL.getId(), "L2-regularized L2-loss support vector classification (dual)"),
            new Tag(SolverType.L2R_L2LOSS_SVC.getId(), "L2-regularized L2-loss support vector classification (primal)"),
            new Tag(SolverType.L2R_L1LOSS_SVC_DUAL.getId(), "L2-regularized L1-loss support vector classification (dual)"),
            new Tag(SolverType.MCSVM_CS.getId(), "support vector classification by Crammer and Singer"),
            new Tag(SolverType.L1R_L2LOSS_SVC.getId(), "L1-regularized L2-loss support vector classification"),
            new Tag(SolverType.L1R_LR.getId(), "L1-regularized logistic regression"),
            new Tag(SolverType.L2R_LR_DUAL.getId(), "L2-regularized logistic regression (dual)"),
            new Tag(SolverType.L2R_L2LOSS_SVR.getId(), "L2-regularized L2-loss support vector regression (primal)"),
            new Tag(SolverType.L2R_L2LOSS_SVR_DUAL.getId(), "L2-regularized L2-loss support vector regression (dual)"),
            new Tag(SolverType.L2R_L1LOSS_SVR_DUAL.getId(), "L2-regularized L1-loss support vector regression (dual)")};

    protected final SolverType     DEFAULT_SOLVER         = SolverType.L2R_L2LOSS_SVC_DUAL;

    /** the SVM solver type */
    protected SolverType           m_SolverType           = DEFAULT_SOLVER;

    /** stopping criteria */
    protected double               m_eps                  = 0.001;

    /** epsilon of epsilon-insensitive cost function **/
    protected double m_epsilon = 1e-1;

    /** cost Parameter C */
    protected double               m_Cost                 = 1;

    /** the maximum number of iterations */
    protected int m_MaxIts = 1000;

    /** bias term value */
    protected double               m_Bias                 = 1;

    protected int[]                m_WeightLabel          = new int[0];

    protected double[]             m_Weight               = new double[0];

    /** whether to generate probability estimates instead of +1/-1 in case of
     * classification problems */
    protected boolean              m_ProbabilityEstimates = false;
    
    /**
     * List used to store class values of instances to be removed
     * in RemoveWithValues filter
     */
    private ArrayList<int[]> removeIndices;
    
    /**
     * Helper function to find subsets of size r in array of size n
     * 
     * @param arr		Array to find subsets
     * @param n			Size of array arr
     * @param r			Size of subsets
     * @param index		Current index in data subset
     * @param data		Subset
     * @param i			Current index in arr array
     */
    private void findIndicesToRemoveUtil(int arr[], int n, int r,
            int index, int data[], int i) {
		// Current subset is found
		if (index == r) {
			removeIndices.add(data.clone());
			return;
		}
		
		// When no more elements are there to put in data[]
		if (i >= n) return;
		
		// current is included, put next at next location
		data[index] = arr[i];
		findIndicesToRemoveUtil(arr, n, r, index + 1, data, i + 1);
		
		// current is excluded, replace it with next 
		// (Note that i+1 is passed, but index is not changed)
		findIndicesToRemoveUtil(arr, n, r, index, data, i + 1);
	}
	
	/**
	 * Function that finds all subsets of size r in arr[] of size n. 
	 *
	 * @param arr Array of class values in which we find subsets
	 * @param n   Size of arr array
	 * @param r   Size of subsets
	 */
	private void findIndicesToRemove(int arr[], int n, int r) {
		int data[] = new int[r];
		
		// Find all subsets using temporary array 'data[]'
		findIndicesToRemoveUtil(arr, n, r, 0, data, 0);
	}

	/**
	 * Builds the classifier.
	 * <br>
	 * We build three binary classifiers. 
	 * In order to build each of them, we need to remove instances with the unnecessary class and adjust class attribute.
	 * In the second part we build optimal stack table
	 * <br>
	 *
     * @param instances   The training data
     * @throws Exception  if nbsvm class encounters a problem or if the filter doesn't get applied properly
     */ 
	@Override
	public void buildClassifier(Instances instances) throws Exception {
		
		getCapabilities().testWithFail(instances);
		
		// remove instances with missing class
	    instances = new Instances(instances);
	    instances.deleteWithMissingClass();
	        
	    numClasses = instances.numClasses();

	    //number of needed binary classifiers
	    numModels = numClasses*(numClasses-1)/2;
	    models = new LibLINEAR[numModels];
		for (int i = 0; i < numModels; i++)
			models[i] = new LibLINEAR();
		
		//set options to liblinear models
    	for (int i = 0; i < numModels; i++)
    		models[i].setOptions(getOptions());
	    
		//if number of classes is 2 then optimal stacks technique is not needed
	    if (numClasses == 2) {
	    	models[0].buildClassifier(instances);
	    }
	    else {
	    	classAttr = instances.classAttribute();
		    int classAttrIndex = classAttr.index();	       
		    
		    optStackTableSize = (int) Math.pow(2, numModels);
		    optimalStackTable = new int[optStackTableSize];
		    
		    /*
			 * According to paper The importance of neutral example for learning sentiment
			 * 5-fold cross validation is used to find optimal stacks
			 */
		    Random random = new Random(1);
		    
			instances.randomize(random);
			instances.stratify(NUM_OF_FOLDS);
						
			int[][] originalValues = new int[numClasses][optStackTableSize];
			for (int fold = 0; fold < NUM_OF_FOLDS; fold++) {
				
				Instances trainSet = instances.trainCV(NUM_OF_FOLDS, fold, random);
				
				RemoveWithValues removeFilter = new RemoveWithValues();
				//set class attribute
			    removeFilter.setAttributeIndex(new Integer(classAttrIndex+1).toString());
			    removeFilter.setModifyHeader(true);
			    
			    removeIndices = new ArrayList<>();
			    int[] ind = IntStream.range(0, numClasses).toArray();
			    findIndicesToRemove(ind, numClasses, numClasses - 2);
			    
			    //remove instances with unnecessary class values and train models
				for (int i = 0; i < numModels; i++) {	    
				    removeFilter.setNominalIndicesArr(removeIndices.get(i));  
				    removeFilter.setInputFormat(trainSet);
					Instances filtered = Filter.useFilter(trainSet, removeFilter); 
					
					models[i].buildClassifier(filtered);			
				}
			   	    
				Instances testSet = instances.testCV(NUM_OF_FOLDS, fold);	
				
				for (Instance instance: testSet) {
					double[][] predictions = new double[numModels][];
					
					for (int i = 0; i < numModels; i++) {
						predictions[i] = models[i].distributionForInstance(instance);	
					}
					
					int index = 0;
					for (int i = 0; i < numModels; i++) {
						index |= (predictions[i][0]>predictions[i][1]?0:1)<<i;
					}
					
					originalValues[(int)instance.classValue()][index]++;
				}				
			}
			
			
			for (int i = 0; i < optStackTableSize; i++) {
				int largest = 0;
				for (int j = 1; j < numClasses; j++) {
					if (originalValues[j][i]>originalValues[largest][i]) 
						largest = j;
						
				}
				optimalStackTable[i] = largest;
				
			}
	    } 
		
	}
	
	/**
     * Classifies a given instance.
     * <br>
     * 
     * @param instance The instance to be classified
     * @return Class
     * @throws Exception If the classifying can't be computed successfully
     */
	@Override
	public double classifyInstance(Instance instance) throws Exception {

		if (numClasses == 2) {
			double[] prediction = models[0].distributionForInstance(instance);
			return prediction[0]>prediction[1]?0:1;
		}
		else {
			double[][] predictions = new double[numModels][];
			
			for (int i = 0; i < numModels; i++) {
				predictions[i] = models[i].distributionForInstance(instance);	
			}
			
			//compare probabilities in order to obtain the order
			//use the order to read the optimal stack table
			int index = 0;
			for (int i = 0; i < numModels; i++) {
				index |= (predictions[i][0]>predictions[i][1]?0:1)<<i;
			}
			
			return optimalStackTable[index];		
		}
    }
    
	 /**
     * Returns an enumeration describing the available options.
     *
     * @return an enumeration of all the available options.
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public Enumeration listOptions() {

        Vector<Object> result = new Vector<Object>();

        result.addElement(new Option("\tSet type of solver (default: 1)\n" //
            + "\tfor multi-class classification\n" //
            + "\t\t 0 -- L2-regularized logistic regression (primal)\n" //
            + "\t\t 1 -- L2-regularized L2-loss support vector classification (dual)\n" //
            + "\t\t 2 -- L2-regularized L2-loss support vector classification (primal)\n" //
            + "\t\t 3 -- L2-regularized L1-loss support vector classification (dual)\n" //
            + "\t\t 4 -- support vector classification by Crammer and Singer\n" //
            + "\t\t 5 -- L1-regularized L2-loss support vector classification\n" //
            + "\t\t 6 -- L1-regularized logistic regression\n" //
            + "\t\t 7 -- L2-regularized logistic regression (dual)\n" //
            + "\tfor regression\n" //
            + "\t\t11 -- L2-regularized L2-loss support vector regression (primal)\n" //
            + "\t\t12 -- L2-regularized L2-loss support vector regression (dual)\n" //
            + "\t\t13 -- L2-regularized L1-loss support vector regression (dual)", //
            "S", 1, "-S <int>"));

        result.addElement(new Option("\tSet the cost parameter C\n" + "\t (default: 1)", "C", 1, "-C <double>"));

        result.addElement(new Option("\tTurn on normalization of input data (default: off)", "Z", 0, "-Z"));

        result.addElement(new Option("\tUse probability estimation (default: off)\n"
            + "currently for L2-regularized logistic regression, L1-regularized logistic regression or L2-regularized logistic regression (dual)! ", "P", 0,
            "-P"));

        result.addElement(new Option("\tSet tolerance of termination criterion (default: 0.001)", "E", 1, "-E <double>"));

        result.addElement(new Option("\tSet the parameters C of class i to weight[i]*C\n" + "\t (default: 1)", "W", 1, "-W <double>"));

        result.addElement(new Option("\tAdd Bias term with the given value if >= 0; if < 0, no bias term added (default: 1)", "B", 1, "-B <double>"));

        result.addElement(new Option(
                "\tThe epsilon parameter in epsilon-insensitive loss function.\n"
                        + "\t(default 0.1)", "L", 1, "-L <double>"));

        result.addElement(new Option(
                "\tThe maximum number of iterations to perform.\n"
                        + "\t(default 1000)", "I", 1, "-I <int>"));

        Enumeration en = super.listOptions();
        while (en.hasMoreElements())
            result.addElement(en.nextElement());

        return result.elements();
    }

    /**
     * Sets the classifier options <p/>
     *
     <!-- options-start -->
     * Valid options are: <p/>
     *
     * <pre> -S &lt;int&gt;
     *  Set type of solver (default: 1)
     *   for multi-class classification
     *     0 -- L2-regularized logistic regression (primal)
     *     1 -- L2-regularized L2-loss support vector classification (dual)
     *     2 -- L2-regularized L2-loss support vector classification (primal)
     *     3 -- L2-regularized L1-loss support vector classification (dual)
     *     4 -- support vector classification by Crammer and Singer
     *     5 -- L1-regularized L2-loss support vector classification
     *     6 -- L1-regularized logistic regression
     *     7 -- L2-regularized logistic regression (dual)
     *  for regression
     *    11 -- L2-regularized L2-loss support vector regression (primal)
     *    12 -- L2-regularized L2-loss support vector regression (dual)
     *    13 -- L2-regularized L1-loss support vector regression (dual)</pre>
     *
     * <pre> -C &lt;double&gt;
     *  Set the cost parameter C
     *   (default: 1)</pre>
     *
     * <pre> -Z
     *  Turn on normalization of input data (default: off)</pre>
     *
     * <pre>
     * -L &lt;double&gt;
     *  The epsilon parameter in epsilon-insensitive loss function.
     *  (default 0.1)
     * </pre>
     *
     * <pre>
     * -I &lt;int&gt;
     *  The maximum number of iterations to perform.
     *  (default 0.1)
     * </pre>
     *
     * <pre> -P
     *  Use probability estimation (default: off)
     * currently for L2-regularized logistic regression only! </pre>
     *
     * <pre> -E &lt;double&gt;
     *  Set tolerance of termination criterion (default: 0.001)</pre>
     *
     * <pre> -W &lt;double&gt;
     *  Set the parameters C of class i to weight[i]*C
     *   (default: 1)</pre>
     *
     * <pre> -B &lt;double&gt;
     *  Add Bias term with the given value if &gt;= 0; if &lt; 0, no bias term added (default: 1)</pre>
     *
     * <pre> -D
     *  If set, classifier is run in debug mode and
     *  may output additional info to the console</pre>
     *
     <!-- options-end -->
     *
     * @param options     the options to parse
     * @throws Exception  if parsing fails
     */
    public void setOptions(String[] options) throws Exception {    	
        String tmpStr;

        tmpStr = Utils.getOption('S', options);
        if (tmpStr.length() != 0)
            setSVMType(new SelectedTag(Integer.parseInt(tmpStr), TAGS_SVMTYPE));
        else
            setSVMType(new SelectedTag(DEFAULT_SOLVER.getId(), TAGS_SVMTYPE));

        tmpStr = Utils.getOption('C', options);
        if (tmpStr.length() != 0)
            setCost(Double.parseDouble(tmpStr));
        else
            setCost(1);

        tmpStr = Utils.getOption('E', options);
        if (tmpStr.length() != 0)
            setEps(Double.parseDouble(tmpStr));
        else
            setEps(0.001);

        setNormalize(Utils.getFlag('Z', options));

        tmpStr = Utils.getOption('B', options);
        if (tmpStr.length() != 0)
            setBias(Double.parseDouble(tmpStr));
        else
            setBias(1);

        setWeights(Utils.getOption('W', options));

        setProbabilityEstimates(Utils.getFlag('P', options));

        tmpStr = Utils.getOption('L', options);
        if (tmpStr.length() != 0) {
            setEpsilonParameter(Double.parseDouble(tmpStr));
        } else {
            setEpsilonParameter(0.1);
        }

        tmpStr = Utils.getOption('I', options);
        if (tmpStr.length() != 0) {
            setMaximumNumberOfIterations(Integer.parseInt(tmpStr));
        } else {
            setMaximumNumberOfIterations(1000);
        }

        super.setOptions(options);
    }

    /**
     * Returns the current options
     *
     * @return            the current setup
     */
    public String[] getOptions() {

        List<String> options = new ArrayList<String>();

        options.add("-S");
        options.add("" + m_SolverType.getId());

        options.add("-C");
        options.add("" + getCost());

        options.add("-E");
        options.add("" + getEps());

        options.add("-B");
        options.add("" + getBias());

        if (getNormalize()) options.add("-Z");

        if (getWeights().length() != 0) {
            options.add("-W");
            options.add("" + getWeights());
        }

        if (getProbabilityEstimates()) options.add("-P");

        options.add("-L");
        options.add("" + getEpsilonParameter());

        options.add("-I");
        options.add("" + getMaximumNumberOfIterations());

        return options.toArray(new String[options.size()]);
    }
    
    /**
     * Sets type of SVM (default SVMTYPE_L2)
     *
     * @param value       the type of the SVM
     */
    public void setSVMType(SelectedTag value) {
        if (value.getTags() == TAGS_SVMTYPE) {
            setSolverType(SolverType.getById(value.getSelectedTag().getID()));
        }
    }

    protected void setSolverType(SolverType solverType) {
        m_SolverType = solverType;
    }

    protected SolverType getSolverType() {
        return m_SolverType;
    }

    /**
     * Gets type of SVM
     *
     * @return            the type of the SVM
     */
    public SelectedTag getSVMType() {
        return new SelectedTag(m_SolverType.getId(), TAGS_SVMTYPE);
    }

    /**
     * Returns the tip text for this property
     *
     * @return tip text for this property suitable for
     *         displaying in the explorer/experimenter gui
     */
    public String SVMTypeTipText() {
        return "The type of SVM to use.";
    }

    /**
     * Get the value of epsilon parameter of the epsilon insensitive loss
     * function.
     *
     * @return Value of epsilon parameter.
     */
    public double getEpsilonParameter() {
        return m_epsilon;
    }

    /**
     * Set the value of epsilon parameter of the epsilon insensitive loss
     * function.
     *
     * @param v Value to assign to epsilon parameter.
     */
    public void setEpsilonParameter(double v) {
        m_epsilon = v;
    }

    /**
     * Returns the tip text for this property
     *
     * @return tip text for this property suitable for displaying in the
     *         explorer/experimenter gui
     */
    public String epsilonParameterTipText() {
        return "The epsilon parameter of the epsilon insensitive loss function.";
    }

    /**
     * Get the number of iterations to perform.
     *
     * @return maximum number of iterations to perform.
     */
    public int getMaximumNumberOfIterations() {
        return m_MaxIts;
    }

    /**
     * Set the number of iterations to perform.
     *
     * @param v the number of iterations to perform.
     */
    public void setMaximumNumberOfIterations(int v) {
        m_MaxIts = v;
    }

    /**
     * Returns the tip text for this property
     *
     * @return tip text for this property suitable for displaying in the
     *         explorer/experimenter gui
     */
    public String maximumNumberOfIterationsTipText() {
        return "The maximum number of iterations to perform.";
    }

    /**
     * Sets the cost parameter C (default 1)
     *
     * @param value       the cost value
     */
    public void setCost(double value) {
        m_Cost = value;
    }

    /**
     * Returns the cost parameter C
     *
     * @return            the cost value
     */
    public double getCost() {
        return m_Cost;
    }

    /**
     * Returns the tip text for this property
     *
     * @return tip text for this property suitable for
     *         displaying in the explorer/experimenter gui
     */
    public String costTipText() {
        return "The cost parameter C.";
    }

    /**
     * Sets tolerance of termination criterion (default 0.001)
     *
     * @param value       the tolerance
     */
    public void setEps(double value) {
        m_eps = value;
    }

    /**
     * Gets tolerance of termination criterion
     *
     * @return            the current tolerance
     */
    public double getEps() {
        return m_eps;
    }

    /**
     * Returns the tip text for this property
     *
     * @return tip text for this property suitable for
     *         displaying in the explorer/experimenter gui
     */
    public String epsTipText() {
        return "The tolerance of the termination criterion.";
    }

    /**
     * Sets bias term value (default 1)
     * No bias term is added if value &lt; 0
     *
     * @param value       the bias term value
     */
    public void setBias(double value) {
        m_Bias = value;
    }

    /**
     * Returns bias term value (default 1)
     * No bias term is added if value &lt; 0
     *
     * @return             the bias term value
     */
    public double getBias() {
        return m_Bias;
    }

    /**
     * Returns the tip text for this property
     *
     * @return tip text for this property suitable for
     *         displaying in the explorer/experimenter gui
     */
    public String biasTipText() {
        return "If >= 0, a bias term with that value is added; " + "otherwise (<0) no bias term is added (default: 1).";
    }

    /**
     * Returns the tip text for this property
     *
     * @return tip text for this property suitable for
     *         displaying in the explorer/experimenter gui
     */
    public String normalizeTipText() {
        return "Whether to normalize the data.";
    }

    /**
     * whether to normalize input data
     *
     * @param value       whether to normalize the data
     */
    public void setNormalize(boolean value) {
        m_Normalize = value;
    }

    /**
     * whether to normalize input data
     *
     * @return            true, if the data is normalized
     */
    public boolean getNormalize() {
        return m_Normalize;
    }

    /**
     * Sets the parameters C of class i to weight[i]*C (default 1).
     * Blank separated list of doubles.
     *
     * @param weightsStr          the weights (doubles, separated by blanks)
     */
    public void setWeights(String weightsStr) {
        StringTokenizer tok = new StringTokenizer(weightsStr, " ");
        m_Weight = new double[tok.countTokens()];
        m_WeightLabel = new int[tok.countTokens()];

        if (m_Weight.length == 0) System.out.println("Zero Weights processed. Default weights will be used");

        for (int i = 0; i < m_Weight.length; i++) {
            m_Weight[i] = Double.parseDouble(tok.nextToken());
            m_WeightLabel[i] = i;
        }
    }

    /**
     * Gets the parameters C of class i to weight[i]*C (default 1).
     * Blank separated doubles.
     *
     * @return            the weights (doubles separated by blanks)
     */
    public String getWeights() {

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < m_Weight.length; i++) {
            if (i > 0) sb.append(" ");
            sb.append(m_Weight[i]);
        }

        return sb.toString();
    }

    /**
     * Returns the tip text for this property
     *
     * @return tip text for this property suitable for
     *         displaying in the explorer/experimenter gui
     */
    public String weightsTipText() {
        return "The weights to use for the classes, if empty 1 is used by default.";
    }

    /**
     * Returns whether probability estimates are generated instead of -1/+1 for
     * classification problems.
     *
     * @param value       whether to predict probabilities
     */
    public void setProbabilityEstimates(boolean value) {
        m_ProbabilityEstimates = value;
    }

    /**
     * Sets whether to generate probability estimates instead of -1/+1 for
     * classification problems.
     *
     * @return            true, if probability estimates should be returned
     */
    public boolean getProbabilityEstimates() {
        return m_ProbabilityEstimates;
    }

    /**
     * Returns the tip text for this property
     *
     * @return tip text for this property suitable for
     *         displaying in the explorer/experimenter gui
     */
    public String probabilityEstimatesTipText() {
        return "Whether to generate probability estimates instead of -1/+1 for classification problems "
            + "(currently for L2-regularized logistic regression only!)";
    }


	/**
     * Returns a string describing classifier
     *
     * @return A description suitable for displaying in the
     *         explorer/experimenter gui
     */
    public String globalInfo() {
		return "An optimal stack of LibLINEAR binary classifiers.\n\n";
	}
    
    
    /**
    * Returns default capabilities of the classifier.
    *
    * @return the capabilities of this classifier
    */
    @Override
    public Capabilities getCapabilities() {   
	    return (new LibLINEAR()).getCapabilities();
    }
    
    /**
     * Returns a string representation of the model including optimal stack table
     *
     * @return A string representation
     */
    @Override
    public String toString() {
    	StringBuffer sb = new StringBuffer();
    	sb.append("Weka's Optimal Stack of LibLINEAR binary classifiers implementation\n\n");
    	
    	if (numClasses > 2) {
    		sb.append("=== Optimal stack table ===\n");
         	
        	for (int i = 0; i < numModels; i++) {
        		int[] toRemove = removeIndices.get(i);
        		int[] toKeep = findIndices(IntStream.range(0, numClasses).toArray(), toRemove);
        		sb.append(classAttr.value(toKeep[0])+"/"+classAttr.value(toKeep[1])+" ");  		
        	}
        	sb.append("\tresult\n");
        	for (int i = 0; i < optStackTableSize; i++) {
        		int[] predictions = new int[numModels];
        		sb.append("\t");
        		for (int j = 0; j < numModels; j++) {
        			predictions[j] = (i>>j) & 1;
        			sb.append(predictions[j]+"\t\t");
        		}
        		
        		sb.append(classAttr.value(optimalStackTable[i])+"\n");
        	} 	
        	        		
    	}

		return sb.toString();
    }
    
    /**
     * @param arr		sorted array of class values 0..numClasses-1
     * @param toRemove	sorted array of class values to remove from arr
     * @return 			remaining values
     */
    private int[] findIndices(int[] arr, int[] toRemove) {
    	int[] result = new int[2];
    	int iRem = 0, iRes = 0;
    	for (int i = 0; i < arr.length; i++) {
    		if (iRem < toRemove.length && arr[i] != toRemove[iRem]) {
    			result[iRes++] = arr[i];
    		}
    		else if (iRem == toRemove.length) {
    			while (i < arr.length) result[iRes++] = arr[i++];
    		}
    		else {
    			iRem++;
    		}
    	}
    	return result;
	}
    
    /**
     * Returns the revision string.
     *
     * @return The revision
     */
    @Override
    public String getRevision() {
    	return REVISION;
    }
}
